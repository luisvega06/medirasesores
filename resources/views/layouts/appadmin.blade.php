<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>@yield('title') - Tigo</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>      
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <link type="text/css"  rel="stylesheet" href="{{ asset('css/materialize.css') }}" media="screen,projection"/>
    <link type="text/css"  rel="stylesheet" href="{{ asset('css/animate.css') }}" media="screen,projection"/>
    
</head>
<body class="@yield('style_body')">
    
 <!-- inicio de navbar -->
    
    <nav>
        <div class="nav-wrapper container">
          <a href="#" class="brand-logo">Logo</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="\create_supervisor">crear supervisor</a></li>
            <li><a href="/empresas_admin">Empresas</a></li>
            <li><a href="/crear_empresa">Crear empresa</a></li>
            <li><a href="/crear_canal">Crear Canales</a></li>
        </ul>
    </div>
</nav>
    


    <!-- fin de navbar -->
    
    <!--Agrega contenido  -->
    @yield('content')

   

    <!-- script -->
    <script src="{{ asset('js/jquery-3.4.0.min.js' ) }}"></script>
    <script src="{{ asset('js/materialize.min.js'  ) }}"></script>
    <script src="{{ asset('js/sweetalert.min.js'   ) }}"></script>

    <script>
        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
            M.updateTextFields();
            $('select').formSelect();
            // $('.datepicker').datepicker();
            $('.datepicker').datepicker({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15, // Creates a dropdown of 15 years to control year
                format: 'yyyy-mm-dd' 
            });
            $('.timepicker').timepicker({
                default: 'now',
                twelveHour: false, // change to 12 hour AM/PM clock from 24 hour
                donetext: 'OK',
                // format: "H:MM:SS",
                autoclose: false,
                vibrate: true
            });
            $('.tooltipped').tooltip();
        });
    </script>

    @yield('extra_scripts')

</body>
</html>
