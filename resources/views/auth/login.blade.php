@extends('layouts.applogin')
@section('title', "Login")

@section('style_body', 'grey lighten-2')
@section('content')


    <div class="white col s12 m8 l6 xl6 offset-m2 offset-l3 offset-xl3 hoverable z-depth-5">
        <form role="form" method="POST" action="">                
            {{ csrf_field() }}
            <div class="row"><br>
                <div class="col m8 s8 offset-m2 offset-s2 center">
                    <h4 class="truncate bg-card-user">
                        <img src="https://images-na.ssl-images-amazon.com/images/I/71yts-LQ9ZL.png" alt="" class="circle responsive-img">
                        <div class="row login">
                            <h4><b>Inicia sesión</b></h4>
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field col m12 s12">
                                        <i class="material-icons iconis prefix">account_box</i>
                                        <input id="icon_prefix" id="email" name="email" type="email" class="validate" value="{{ old('email') }}">
                                        <label for="icon_prefix">Nombre o Email</label>
                                    </div>

                                    @if ($errors->has('email'))
                                        <span>
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif

                                </div>
                                <div class="row">
                                    <div class="input-field col m12 s12">
                                        <i class="material-icons iconis prefix">enhanced_encryption</i>
                                        <input id="password" name="password" type="password" class="validate">
                                        <label for="password">Contraseña</label>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span>
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="row">
                                    <button class="btn waves-effect waves-light" type="submit" name="action">Iniciar sesión!</button>
                                </div>
                            </div>
                        </div>
                    </h4>
                </div>
            </div>        
        </form>
    </div>
    


@endsection


@section('extra_scripts')

    <script type="text/javascript">

        
        var mensaje = "";
        @if ($errors->any())
            @foreach($errors->all() as $error)
            mensaje += '{{ $error }}' + '\n';
            @endforeach

            alert(" Mensaje: ",  mensaje);

        @endif
    </script>

@endsection
