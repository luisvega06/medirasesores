@extends('layouts.app')
@section('title', "Registrar")


@section('content')

<div class="panel-heading center"><h4>Crear tu cuenta</h4></div>
<br>
<br>

<div class="row container">

    <form class="form-horizontal" method="POST" action="">
        {{ csrf_field() }}

        <div class="form-group col s6{{ $errors->has('canal_id') ? ' has-error' : '' }}">
            <div class="input-field ">
                <select name="canal_id">
                    @foreach ($canales as $canal )
                    <option value="{{$canal->id}}">{{ $canal->name }}</option>
                    @endforeach  
                </select>
                <label>Elige tu canal</label>
            </div>
        </div>



        <div class="form-group col s6{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="col-md-4 control-label">Celular</label>

            <div class="col-md-6">
                <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                @if ($errors->has('phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('codigosupervisor') ? ' has-error' : '' }}">
            <label for="codigosupervisor" class="col-md-4 control-label">Codigo Supervisor</label>

            <div class="col-md-6">
                <input id="codigosupervisor" type="text" class="form-control" name="codigosupervisor" value="{{ old('codigosupervisor') }}" required autofocus>

                @if ($errors->has('codigosupervisor'))
                <span class="help-block">
                    <strong>{{ $errors->first('codigosupervisor') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Name</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6">
            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>
        <br>

        <div class="form-group col s6">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>
    </form>
</div>



@endsection
