@extends('layouts.app')
@section('title', "Bienvenido (a)")

@section('style_body', 'grey lighten-2')
@section('content')

<div class="row">
    <a class="waves-effect waves-light btn blue darken-3 btn-radius" href="{{ url('/login')}}">
        Iniciar Sesión
    </a>

    <a class="waves-effect waves-light btn blue darken-3 btn-radius" href="{{ url('/register') }}">
        Crear Cuenta
    </a>

    <a class="waves-effect waves-light btn blue darken-3 btn-radius"  href="{{ route('logout') }}"
    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    	Logout
	</a>


    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    	{{ csrf_field() }}
    </form>
</div>


@endsection