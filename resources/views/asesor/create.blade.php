@extends('layouts.app')

@section('content')

<h4 class="center">
	<b>Crear Referido</b>
</h4>
<br>

<form method="POST" action="/asesores">
	{{ csrf_field() }}

	<div style="display : none">
		<input type="number" name="form_id" value="1">
	</div>
	

	<div class="row" id="etapa_1">
		<div class="input-field col s6">
			<input id="nombre" type="text" class="validate" name="nombre">
			<label class="active" for="nombre">Nombre Completo</label>
		</div>

		<div class="input-field col s6">
			<input id="cedula" type="text" class="validate" name="cedula">
			<label class="active" for="cedula">Cédula</label>
		</div>

		<div class="input-field col s6">
			<input id="telefono" type="tel" class="validate" name="telefono">
			<label class="active" for="telefono">telefono</label>
		</div>

		<div class="input-field col s6">
			<input id="direccion" type="text" class="validate" name="direccion">
			<label class="active" for="direccion">Dirección</label>
		</div>

		<div class="input-field col s6">
			<input id="correo" type="email" class="validate" name="correo">
			<label class="active" for="correo">Correo</label>
		</div>

		<div class="input-field col s6">
			<select id="oferta" name="oferta">
				<option value="1">Prepago</option>
				<option value="2">PosPago</option>
				<option value="3">Portabilidad</option>
			</select>
			<label>Oferta</label>
		</div>

		<div class="col s6">
			<p>¿Llevara celular?</p>
			
				<p>
					<label>
						<input id="concelular1" name="concelular" type="radio" />
						<span> Si </span>
					</label>
				</p>
				<p>
					<label>
						<input id="concelular2" name="concelular" type="radio" />
						<span> No </span>
					</label>
				</p>
				
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="next_1()">
				Continuar
			</button>
		</div>

	</div>


	{{-- PosPago & Con Celular --}}
	<div class="row" id="etapa_2_pospago_plan" style="display : none">
		<div class="input-field col s12" id="content_data_celular1">
			<input id="presupuesto_etapa2" type="number" class="validate" name="presupuesto_etapa2" required>
			<label class="active" for="presupuesto"> Presupuesto </label>
		</div>

		<div class="input-field col s12" id="content_data_celular2">
			<select id="modelo_etapa2" name="modelo_etapa2" required>
				<option value="1">Samsung</option>
				<option value="2">LG</option>
				<option value="3">Motorola</option>
			</select>
			<label> Modelo Smartphone </label>
		</div>

		<div class="input-field col s12">
			<select id="plan_etapa2" name="plan" required>
				<option value="1">Plan A</option>
				<option value="2">PLan B</option>
				<option value="3">Plan C</option>
			</select>
			<label> Planes de teléfonia </label>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="agendarllevar_etapa2" id="llevarHoy_etapa2" type="radio" />
					<span> Llevar dispositivo </span>
				</label>
			</p>
			<p>
				<label>
					<input name="agendarllevar_etapa2" id="agendar_etapa2" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa2" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" name="fecha_etapa2" id="fecha_etapa2">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" name="hora_etapa2" id="hora_etapa2">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_pospago_plan('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>
	{{-- PosPago & SIN Celular --}}
	<div class="row" id="etapa_21_pospago_plan" style="display : none">
		<div class="input-field col s12" id="content_data_celular21">
			<input id="presupuesto_etapa21" type="number" class="validate" name="presupuesto_etapa21" required>
			<label class="active" for="presupuesto"> Presupuesto </label>
		</div>

		<div class="input-field col s12" id="content_data_celular22">
			<select id="modelo_etapa21" name="modelo_etapa21" required>
				<option value="1">Samsung</option>
				<option value="2">LG</option>
				<option value="3">Motorola</option>
			</select>
			<label> Modelo Smartphone </label>
		</div>

		<div class="input-field col s12">
			<select id="plan_etapa21" name="plan" required>
				<option value="1">Plan A</option>
				<option value="2">PLan B</option>
				<option value="3">Plan C</option>
			</select>
			<label> Planes de teléfonia </label>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="agendarllevar_etapa21" id="llevarHoy_etapa21" type="radio" />
					<span> Activar Plan </span>
				</label>
			</p>
			<p>
				<label>
					<input name="agendarllevar_etapa21" id="agendar_etapa21" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa21" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" name="fecha_etapa21" id="fecha_etapa21">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" name="hora_etapa21" id="hora_etapa21">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_pospago_plan('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>

	{{-- (Prepago ó Portabilidad) CON CELULAR--}}
	<div class="row" id="etapa_3_prepago_portabilidad_con_celular" style="display : none">
		<div class="col s12">
			<p> (Prepago ó Portabilidad) CON CELULAR </p>
		</div>
		<div class="input-field col s12" id="content_data_celular31">
			<input id="presupuesto_etapa3" type="number" class="validate" required>
			<label class="active" for="presupuesto_etapa3"> Presupuesto </label>
		</div>

		<div class="input-field col s12" id="content_data_celular32">
			<select id="modelo_etapa3" name="modelo_etapa3" required>
				<option value="1">Samsung</option>
				<option value="2">LG</option>
				<option value="3">Motorola</option>
			</select>
			<label> Modelo Smartphone </label>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_plan_recarga" id="radio_plan_etapa3" type="radio" />
					<span> Plan de teléfonia </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_plan_recarga" id="radio_recarga_paquete_etapa3" type="radio" />
					<span> Recarga y/ó paquetes tigo </span>
				</label>
			</p>					
		</div>

		<div class="input-field col s12" style="display : none" id="content_plan_etapa3">
			<select id="plan_etapa3" required>
				<option value="1">Plan A</option>
				<option value="2">PLan B</option>
				<option value="3">Plan C</option>
			</select>
			<label> Planes de teléfonia </label>
		</div>
		<div class="input-field col s12" style="display : none" id="content_recarga_etapa3">
			<div class="row">
				<div class="col s12">
					<select id="recarga_etapa3" required>
						<option value="1">Recarga</option>
						<option value="2">Paquete</option>
					</select>
					<label> Recarga y/ó paquetes tigo </label>
					
				</div>
				<div class="input-field col s12">
					<input id="costo_etapa3" type="number" class="validate" required>
					<label class="active" for="costo_etapa3"> Precio </label>
				</div>
			</div>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_llevar_agendar" id="llevarHoy_etapa3" type="radio" />
					<span> Llevar dispositivo </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_llevar_agendar" id="agendar_etapa3" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa3" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" id="fecha_etapa3">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" id="hora_etapa3">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_prepago_portabilidad('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>
	{{-- (Prepago ó Portabilidad) SIN CELULAR--}}
	<div class="row" id="etapa_3_prepago_portabilidad_sin_celular2" style="display : none">
		<div class="col s12">
			<p> (Prepago ó Portabilidad) CON CELULAR </p>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_plan_recarga2" id="radio_plan_etapa32" type="radio" />
					<span> Plan de teléfonia </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_plan_recarga2" id="radio_recarga_paquete_etapa32" type="radio" />
					<span> Recarga y/ó paquetes tigo </span>
				</label>
			</p>					
		</div>

		<div class="input-field col s12" style="display : none" id="content_plan_etapa32">
			<select id="plan_etapa32" required>
				<option value="1">Plan A</option>
				<option value="2">PLan B</option>
				<option value="3">Plan C</option>
			</select>
			<label> Planes de teléfonia </label>
		</div>
		<div class="input-field col s12" style="display : none" id="content_recarga_etapa32">
			<div class="row">
				<div class="col s12">
					<select id="recarga_etapa32" required>
						<option value="1">Recarga</option>
						<option value="2">Paquete</option>
					</select>
					<label> Recarga y/ó paquetes tigo </label>
					
				</div>
				<div class="input-field col s12">
					<input id="costo_etapa32" type="number" class="validate" required>
					<label class="active" for="costo_etapa32"> Precio </label>
				</div>
			</div>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_llevar_agendar2" id="llevarHoy_etapa32" type="radio" />
					<span> Activar </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_llevar_agendar2" id="agendar_etapa32" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa32" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" id="fecha_etapa32">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" id="hora_etapa32">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_prepago_portabilidad('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>


</form>

@endsection

@section('extra_scripts')

	<script>	
		var mensaje = "";
		@if ($errors->any())
			@foreach($errors->all() as $error)
				console.log( '{{ $error }}');
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});
		@endif
		//Etapa 2: Celular pospago - con plan
		$(document).ready(function () {   
			// PosPago - Con Celular
			$("#agendar_etapa2").change(function () {
				var opcion = $("#agendar_etapa2").is(":checked");
				console.log("agendar_etapa2: ", opcion);
				$("#fechaagenda_etapa2").show(200);
			}); 
			$("#llevarHoy_etapa2").change(function () {
				var opcion = $("#llevarHoy_etapa2").is(":checked");
				console.log("llevarHoy_etapa2: ", opcion);
				$("#fechaagenda_etapa2").hide(200);
			}); 

			// PosPago - SIN Celular
			$("#agendar_etapa21").change(function () {
				var opcion = $("#agendar_etapa21").is(":checked");
				console.log("agendar_etapa21: ", opcion);
				$("#fechaagenda_etapa21").show(200);
			}); 
			$("#llevarHoy_etapa21").change(function () {
				var opcion = $("#llevarHoy_etapa21").is(":checked");
				console.log("llevarHoy_etapa21: ", opcion);
				$("#fechaagenda_etapa21").hide(200);
			});


			// PosPago - Con Celular
			$("#agendar_etapa3").change(function () {
				var opcion = $("#agendar_etapa3").is(":checked");
				console.log("agendar_etapa3: ", opcion);
				$("#fechaagenda_etapa3").show(200);
			}); 
			$("#llevarHoy_etapa3").change(function () {
				var opcion = $("#llevarHoy_etapa3").is(":checked");
				console.log("llevarHoy_etapa3: ", opcion);
				$("#fechaagenda_etapa3").hide(200);
			});  

			// Prepago ó Portabilidad - con celular
			$("#radio_plan_etapa3").change(function () {
				var opcion = $("#radio_plan_etapa3").is(":checked");
				console.log("plan_etapa3: ", opcion);
				$("#content_plan_etapa3").show();
				$("#content_recarga_etapa3").hide(200);
			}); 
			$("#radio_recarga_paquete_etapa3").change(function () {
				var opcion = $("#radio_recarga_paquete_etapa3").is(":checked");
				console.log("recarga_paquete_etapa3: ", opcion);
				$("#content_recarga_etapa3").show();
				$("#content_plan_etapa3").hide(200);
			}); 

			// Prepago ó Portabilidad - sin celular

			$("#radio_plan_etapa32").change(function () {
				var opcion = $("#radio_plan_etapa32").is(":checked");
				console.log("plan_etapa32: ", opcion);
				$("#content_plan_etapa32").show();
				$("#content_recarga_etapa32").hide(200);
			}); 
			$("#radio_recarga_paquete_etapa32").change(function () {
				var opcion = $("#radio_recarga_paquete_etapa32").is(":checked");
				console.log("recarga_paquete_etapa32: ", opcion);
				$("#content_recarga_etapa32").show();
				$("#content_plan_etapa32").hide(200);
			});

			$("#agendar_etapa32").change(function () {
				var opcion = $("#agendar_etapa32").is(":checked");
				console.log("agendar_etapa32: ", opcion);
				$("#fechaagenda_etapa32").show(200);
			}); 
			$("#llevarHoy_etapa32").change(function () {
				var opcion = $("#llevarHoy_etapa32").is(":checked");
				console.log("llevarHoy_etapa32: ", opcion);
				$("#fechaagenda_etapa32").hide(200);
			});
		});

		function next_1(){
			var oferta = $("#oferta").val();
			var concelular = $("#concelular1").is(":checked");

			if (oferta == 2 && concelular == true) {
				console.log("\n *Usted elegio PosPago & no lleva celular");
				$("#etapa_1").hide(200);
				$("#etapa_2_pospago_plan").show(500);
			}else if (oferta == 2 && concelular == false) {
				console.log("\n *Usted elegio PosPago & no lleva celular");
				$("#etapa_1").hide(200);
				$("#etapa_21_pospago_plan").show(500);
				$("#content_data_celular21").hide();
				$("#content_data_celular22").hide();
			}else if (oferta != 2 && concelular == true) {
				console.log("\n *Usted [ NO ] elegio [PosPago] & con celular");				
				$("#etapa_1").hide(200);
				$("#etapa_3_prepago_portabilidad_con_celular").show(500);
			}else if (oferta != 2 && concelular == false) {
				console.log("\n *Usted [ NO ] elegio [PosPago] & SIN celular");
				$("#etapa_1").hide(200);
				$("#etapa_3_prepago_portabilidad_sin_celular2").show(500);
			}else {

			}
		}

		function guardar_pospago_plan(_t) {
			var oferta     = $("#oferta").val();
			var concelular = $("#concelular1").is(":checked");

			var caso = 0;
			var data = {};
			if (oferta == 2 && concelular == true) {
				data = {
					_token 				: _t,
					nombre              : $("#nombre").val(),
					cedula              : $("#cedula").val(),
					telefono            : $("#telefono").val(),
					direccion           : $("#direccion").val(),
					correo              : $("#correo").val(),
					oferta              : $("#oferta").val(),
					concelular          : $("#concelular1").is(":checked"),

					presupuesto_etapa   : $("#presupuesto_etapa2").val(),
					modelo_etapa  		: $("#modelo_etapa2").val(),
					plan_etapa  		: $("#plan_etapa2").val(),

					llevarHoy_etapa 	: $("#llevarHoy_etapa2").is(":checked"),
					fecha_etapa 		: $("#fecha_etapa2").val(),
					hora_etapa  		: $("#hora_etapa2").val()
				};
				caso = 1;
			}else{
				if (oferta == 2 && concelular == false) {
					caso = 2;
					data = {
						_token 				: _t,
						nombre              : $("#nombre").val(),
						cedula              : $("#cedula").val(),
						telefono            : $("#telefono").val(),
						direccion           : $("#direccion").val(),
						correo              : $("#correo").val(),
						oferta              : $("#oferta").val(),
						concelular          : false,

						presupuesto_etapa   : 0,
						plan_etapa  		: $("#plan_etapa21").val(),
						llevarHoy_etapa  	: $("#llevarHoy_etapa21").is(":checked"),
						fecha_etapa  		: $("#fecha_etapa21").val(),
						hora_etapa  		: $("#hora_etapa21").val()
					};
				}
			}
			
			console.log("Caso: #", caso);
			console.log("\n Data Model: ", data);

			$.ajax({
				method: 'POST',
				url: '/pospago_celular',
				data: data
				,success: function(data){
					console.log("return: ", data);
				}
				,error: function(data){
					var errors = data.responseJSON;
					console.log('Problemas de conexión con el servidor ' + errors);
				}
			});
		}

		function guardar_prepago_portabilidad(_t) {
			var oferta     = $("#oferta").val();
			var concelular = $("#concelular1").is(":checked");

			console.log("Oferta ID: ", oferta);
			console.log("Con Celular: ", concelular);

			var caso = 0;
			var data = {};
			if (oferta != 2 && concelular == true) {
				if( $("#radio_plan_etapa3").is(":checked") ){
					caso = 0;
					data = {
						_token 				: _t,
						nombre              : $("#nombre").val(),
						cedula              : $("#cedula").val(),
						telefono            : $("#telefono").val(),
						direccion           : $("#direccion").val(),
						correo              : $("#correo").val(),
						oferta              : $("#oferta").val(),
						concelular          : $("#concelular1").is(":checked"),

						presupuesto_etapa   : $("#presupuesto_etapa3").val(),
						modelo_etapa  		: $("#modelo_etapa3").val(),
						plan_etapa  		: $("#plan_etapa3").val(),
						conplan             : $("#radio_plan_etapa3").is(":checked"),

						llevarHoy_etapa 	: $("#llevarHoy_etapa3").is(":checked"),
						fecha_etapa 		: $("#fecha_etapa3").val(),
						hora_etapa  		: $("#hora_etapa3").val()
					};
				} else {
					if( $("#radio_recarga_paquete_etapa3").is(":checked") ){
						caso = 1;
						data = {
							_token 				: _t,
							nombre              : $("#nombre").val(),
							cedula              : $("#cedula").val(),
							telefono            : $("#telefono").val(),
							direccion           : $("#direccion").val(),
							correo              : $("#correo").val(),
							oferta              : $("#oferta").val(),
							concelular          : $("#concelular1").is(":checked"),

							presupuesto_etapa   : $("#presupuesto_etapa3").val(),
							modelo_etapa  		: $("#modelo_etapa3").val(),

							conplan             : $("#radio_plan_etapa3").is(":checked"),
							
							tiporecargapaquete  : $("#recarga_etapa3").val(),
							precio              : $("#costo_etapa3").val(),


							llevarHoy_etapa 	: $("#llevarHoy_etapa3").is(":checked"),
							fecha_etapa 		: $("#fecha_etapa3").val(),
							hora_etapa  		: $("#hora_etapa3").val()
						};
					}
				}
				
			}else{
				if (oferta != 2 && concelular == false) {
					if( $("#radio_plan_etapa32").is(":checked") ){
					caso = 0;
					data = {
						_token 				: _t,
						nombre              : $("#nombre").val(),
						cedula              : $("#cedula").val(),
						telefono            : $("#telefono").val(),
						direccion           : $("#direccion").val(),
						correo              : $("#correo").val(),
						oferta              : $("#oferta").val(),
						concelular          : false,

						presupuesto_etapa   : 0,
						plan_etapa  		: $("#plan_etapa32").val(),
						conplan             : $("#radio_plan_etapa32").is(":checked"),

						llevarHoy_etapa 	: $("#llevarHoy_etapa32").is(":checked"),
						fecha_etapa 		: $("#fecha_etapa32").val(),
						hora_etapa  		: $("#hora_etapa32").val()
					};
				} else {
					if( $("#radio_recarga_paquete_etapa32").is(":checked") ){
						caso = 1;
						data = {
							_token 				: _t,
							nombre              : $("#nombre").val(),
							cedula              : $("#cedula").val(),
							telefono            : $("#telefono").val(),
							direccion           : $("#direccion").val(),
							correo              : $("#correo").val(),
							oferta              : $("#oferta").val(),
							concelular          : false,

							presupuesto_etapa   : 0,

							conplan             : $("#radio_plan_etapa32").is(":checked"),
							
							tiporecargapaquete  : $("#recarga_etapa32").val(),
							precio              : $("#costo_etapa32").val(),


							llevarHoy_etapa 	: $("#llevarHoy_etapa32").is(":checked"),
							fecha_etapa 		: $("#fecha_etapa32").val(),
							hora_etapa  		: $("#hora_etapa32").val()
						};
					}
				}
				}
			}
			
			console.log("PREPAGO Ó PORTABILIDAD Caso: #", caso);
			console.log("PREPAGO Ó PORTABILIDAD Data Model: ", data);

			$.ajax({
				method: 'POST',
				url: '/prepago_portabilidad_celular',
				data: data
				,success: function(data){
					console.log("return: ", data);
				}
				,error: function(data){
					var errors = data.responseJSON;
					console.log('Problemas de conexión con el servidor ' + errors);
				}
			});
		}


	</script>

@endsection