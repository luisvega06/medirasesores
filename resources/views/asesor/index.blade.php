@extends('layouts.app')

@section('title', 'Mis referidos')
@section('content')

<h4 class="center">
	<b>Listado de Prospectos</b>
</h4>
<br>


<div class="container">
    <div class="section">

			

      <!--   Icon Section   -->
      <div class="row">

				@foreach($datos as $data)
					@if($data['oferta']['id'] == 1)
						<div class="col s12 m4">
							<div class="icon-block" style="background: #FF9000; border-radius:25px; height: 100px;">
								<!-- <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2> -->
								<h5 class="center"><strong>{{$data['oferta']['name']}}</strong></h5>

								<h4 class="center">{{$data['total']}}</h4>
							</div>
						</div>
					@endif

					@if($data['oferta']['id'] == 2)
						<div class="col s12 m4">
							<div class="icon-block" style="background: #0297BF; border-radius:25px; height: 100px;">
								<!-- <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2> -->
								<h5 class="center"><strong>{{$data['oferta']['name']}}</strong></h5>

								<h4 class="center">{{$data['total']}}</h4>
							</div>
						</div>
					@endif

					@if($data['oferta']['id'] == 3)
						<div class="col s12 m4">
							<div class="icon-block" style="background: #00BA9B; border-radius:25px; height: 100px;">
								<!-- <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2> -->
								<h5 class="center"><strong>{{$data['oferta']['name']}}</strong></h5>

								<h4 class="center">{{$data['total']}}</h4>
							</div>
						</div>
					@endif

				@endforeach

      </div>

    </div>

<!-- -->
    <div class="container">
        <div class="section">
        <div class="row">
            <div class="col s12 m12">
                <div class="icon-block">
                    <canvas id="grafico"  height="150"></canvas>        
                </div>
            </div>
        
        </div>
    </div>


<!-- -->
    <br><br>
  </div>
@if (sizeof ($modelsRef) > 0)
  <div class="container">
  	
	<table class="highlight responsive-table">
		<thead>
			<tr>
				<th>#    			</th>
				<th>Nombre Completo </th>
				<th>Celular 		</th>
				<th>Correo 			</th>
				<th>Ciudad 			</th>
				<th>Dirección 		</th>
				<th>Fecha    		</th>
				<th>Estado  		</th>
				<th>Acciones		</th>

			</tr>
		</thead>

		<tbody>
			@foreach($modelsRef as $modelref)
			<tr>
					<td> {{ $modelref->referido->id }}         		     </td>
					<td> {{ $modelref->referido->nombre }}      		     </td>
					<td> {{ $modelref->referido->telefono }}                 </td>
					<td> {{ $modelref->referido->correo }}                   </td>					
					<td> {{ $modelref->referido->name }}                     </td>
					<td> {{ $modelref->referido->direccion }}                </td>
					<td> {{ $modelref->referido->created_at }}               </td>
					<td> {{ $modelref->pendiente? "Agendado" : "Activado" }} </td>
					<td>
						<a href="{{ url('/asesores/'.$modelref->referido->id) }}" class="btn-floating btn-small waves-effect waves-light  tooltipped" data-position="bottom" data-tooltip="Mostrar ficha" style="background: #00377B">
							<i class="material-icons">visibility</i>
						</a>
						<a class="btn-floating btn-small waves-effect waves-light  tooltipped" data-position="bottom" data-tooltip="Editar" style="background: #00377B">
							<i class="material-icons">create</i>
						</a>


						
					</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{-- {{$modelsRef->links()}}
  </div> --}}
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js">
    
    </script>
    <script>
        var Prepago={{$datos['0']['total']}};
        var Pospago = {{$datos['1']['total']}};
        var Portabilidad = {{$datos['2']['total']}};
        var mi_primer_grafico ={
            type:"pie",//seleccionamos el tipo de grafico, en este caso es un grafico estilo pie, en esta parte podemos cambiar el tipo de grafico por el que deseamos
            data:{//le pasamos la data
            datasets:[{
                data:[Prepago,Pospago,Portabilidad],//esta es la data, podemos pasarle variables directamente desde el backend usando blade de la siguiente forma {{--$dato1--}},
                backgroundColor: [//seleccionamos el color de fondo para cada dato que le enviamos
                "#FF9000","#0297BF", "#00BA9B",
                ],
            }],
            labels: [//añadimos las etiquetas correspondientes a la data
                "Prepago",  "Pospago", "Portabilidad",  
                ]
            },
            options:{//le pasamos como opcion adicional que sea responsivo
            responsive: true,
            }
        }
        var primer_grafico = document.getElementById('grafico').getContext('2d');//seleccionamos el canvas
        window.pie = new Chart(primer_grafico,mi_primer_grafico);//le pasamos el grafico y la data para representarlo
   
    </script>
@endif


<div class="fixed-action-btn">
	<a class="btn-floating btn-large " href="{{ url('asesores/create') }}" style="background: #00377B">
		<i class="large material-icons">add</i>
	</a>
</div>

@endsection