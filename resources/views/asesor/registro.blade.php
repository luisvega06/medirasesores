@extends('layouts.app')

@section('title', 'Mis referidos')
@section('content')

<h4 class="center">
	<b>Listado de Prospectos</b>
</h4>
<br>


<div class="container">
   



    <br><br>
  </div>
@if (sizeof ($modelsRef) > 0)
  <div class="container">
  	
	<table class="highlight responsive-table">
		<thead>
			<tr>
				<th>#    			</th>
				<th>Nombre Completo </th>
				<th>Celular 		</th>
				<th>Correo 			</th>
				<th>Ciudad 			</th>
				<th>Dirección 		</th>
				<th>Fecha    		</th>
				<th>Estado  		</th>
				<th>Acciones		</th>

			</tr>
		</thead>

		<tbody>
			@foreach($modelsRef as $modelref)
			<tr>
					<td> {{ $modelref->referido->id }}         		     </td>
					<td> {{ $modelref->referido->nombre }}      		     </td>
					<td> {{ $modelref->referido->telefono }}                 </td>
					<td> {{ $modelref->referido->correo }}                   </td>					
					<td> {{ $modelref->referido->name }}                     </td>
					<td> {{ $modelref->referido->direccion }}                </td>
					<td> {{ $modelref->referido->created_at }}               </td>
					<td> {{ $modelref->pendiente? "Agendado" : "Activado" }} </td>
					<td>
						<a href="{{ url('/asesores/'.$modelref->referido->id) }}" class="btn-floating btn-small waves-effect waves-light  tooltipped" data-position="bottom" data-tooltip="Mostrar ficha" style="background: #00377B">
							<i class="material-icons">visibility</i>
						</a>
						<a class="btn-floating btn-small waves-effect waves-light  tooltipped" data-position="bottom" data-tooltip="Editar" style="background: #00377B">
							<i class="material-icons">create</i>
						</a>


						
					</td>
			</tr>
			@endforeach
		</tbody>
	</table>


	@endif


<div class="fixed-action-btn">
	<a class="btn-floating btn-large " href="{{ url('asesores/create') }}" style="background: #00377B">
		<i class="large material-icons">add</i>
	</a>
</div>

@endsection