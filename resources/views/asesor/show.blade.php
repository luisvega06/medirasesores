@extends('layouts.app')
@section('title', 'Referido')

@section('content')

<h4 class="center">
	<b>Crear Referido</b>
</h4>
<br>

<form method="POST" action="/asesores">
	{{ csrf_field() }}	

	<div class="row" id="etapa_1">
		<div class="input-field col s6">
			<input id="nombre" type="text" class="black-text" name="nombre" value="{{ $modelref->referido->nombre }}" disabled="true">
			<label class="active" for="nombre">Nombre Completo</label>
		</div>

		<div class="input-field col s6">
			<input id="cedula" type="text" class="black-text" name="cedula" value="{{ $modelref->referido->cedula }}" disabled="true">
			<label class="active" for="cedula">Cédula</label>
		</div>

		<div class="input-field col s6">
			<input id="telefono" type="tel" class="black-text" name="telefono" value="{{ $modelref->referido->telefono }}" disabled="true">
			<label class="active" for="telefono">telefono</label>
		</div>

		<div class="input-field col s6">
			<input id="direccion" type="text" class="black-text" name="direccion" value="{{ $modelref->referido->direccion }}" disabled="true">
			<label class="active" for="direccion">Dirección</label>
		</div>
		
		<div class="input-field col s6">
			<input id="ciudad" type="text" class="black-text" name="ciudad" value="{{ $modelref->referido->name }}" disabled="true">
			<label class="active" for="direccion">Ciudad</label>
		</div>

		<div class="input-field col s6">
			<input id="correo" type="email" class="black-text" name="correo" value="{{ $modelref->referido->correo }}" disabled="true">
			<label class="active" for="correo">Correo</label>
		</div>

		<div class="input-field col s12">
			<input id="oferta" type="text" class="black-text" name="oferta">
			<label class="active" for="correo">Oferta</label>
		</div>

	</div>


	{{-- PosPago & Con Celular --}}
	<div class="row" id="etapa_2_pospago_plan">
		<div class="input-field col s12" id="content_data_celular1">
			<input id="presupuesto_etapa2" type="number" class="validate" name="presupuesto_etapa2" required>
			<label class="active" for="presupuesto"> Presupuesto </label>
		</div>

		<div class="input-field col s12">
			<input id="modelophone" type="text" class="validate" name="modelophone">
			<label class="active" for="correo">Modelo Celular</label>
		</div>

		<div class="input-field col s12">
			<input id="plan" type="text" class="validate" name="plan">
			<label class="active" for="correo">Plan</label>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="agendarllevar_etapa2" id="llevarHoy_etapa2" type="radio" />
					<span> Llevar dispositivo </span>
				</label>
			</p>
			<p>
				<label>
					<input name="agendarllevar_etapa2" id="agendar_etapa2" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa2" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" name="fecha_etapa2" id="fecha_etapa2">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" name="hora_etapa2" id="hora_etapa2">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_pospago_plan('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>
	{{-- PosPago & SIN Celular --}}
	<div class="row" id="etapa_21_pospago_plan" style="display : none">
		<div class="input-field col s12" id="content_data_celular21">
			<input id="presupuesto_etapa21" type="number" class="validate" name="presupuesto_etapa21" required>
			<label class="active" for="presupuesto"> Presupuesto </label>
		</div>

		<div class="input-field col s12" id="content_data_celular22">
			<select id="modelo_etapa21" name="modelo_etapa21" required>
				<option value="1">Samsung</option>
				<option value="2">LG</option>
				<option value="3">Motorola</option>
			</select>
			<label> Modelo Smartphone </label>
		</div>

		<div class="input-field col s12">
			<select id="plan_etapa21" name="plan" required>
				<option value="1">Plan A</option>
				<option value="2">PLan B</option>
				<option value="3">Plan C</option>
			</select>
			<label> Planes de teléfonia </label>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="agendarllevar_etapa21" id="llevarHoy_etapa21" type="radio" />
					<span> Activar Plan </span>
				</label>
			</p>
			<p>
				<label>
					<input name="agendarllevar_etapa21" id="agendar_etapa21" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa21" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" name="fecha_etapa21" id="fecha_etapa21">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" name="hora_etapa21" id="hora_etapa21">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_pospago_plan('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>

	{{-- (Prepago ó Portabilidad) CON CELULAR--}}
	<div class="row" id="etapa_3_prepago_portabilidad_con_celular" style="display : none">
		<div class="col s12">
			<p> (Prepago ó Portabilidad) CON CELULAR </p>
		</div>
		<div class="input-field col s12" id="content_data_celular31">
			<input id="presupuesto_etapa3" type="number" class="validate" required>
			<label class="active" for="presupuesto_etapa3"> Presupuesto </label>
		</div>

		<div class="input-field col s12" id="content_data_celular32">
			<select id="modelo_etapa3" name="modelo_etapa3" required>
				<option value="1">Samsung</option>
				<option value="2">LG</option>
				<option value="3">Motorola</option>
			</select>
			<label> Modelo Smartphone </label>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_plan_recarga" id="radio_plan_etapa3" type="radio" />
					<span> Plan de teléfonia </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_plan_recarga" id="radio_recarga_paquete_etapa3" type="radio" />
					<span> Recarga y/ó paquetes tigo </span>
				</label>
			</p>					
		</div>

		<div class="input-field col s12" style="display : none" id="content_plan_etapa3">
			<select id="plan_etapa3" required>
				<option value="1">Plan A</option>
				<option value="2">PLan B</option>
				<option value="3">Plan C</option>
			</select>
			<label> Planes de teléfonia </label>
		</div>
		<div class="input-field col s12" style="display : none" id="content_recarga_etapa3">
			<div class="row">
				<div class="col s12">
					<select id="recarga_etapa3" required>
						<option value="1">Recarga</option>
						<option value="2">Paquete</option>
					</select>
					<label> Recarga y/ó paquetes tigo </label>
					
				</div>
				<div class="input-field col s12">
					<input id="costo_etapa3" type="number" class="validate" required>
					<label class="active" for="costo_etapa3"> Precio </label>
				</div>
			</div>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_llevar_agendar" id="llevarHoy_etapa3" type="radio" />
					<span> Llevar dispositivo </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_llevar_agendar" id="agendar_etapa3" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa3" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" id="fecha_etapa3">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" id="hora_etapa3">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_prepago_portabilidad('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>
	{{-- (Prepago ó Portabilidad) SIN CELULAR--}}
	<div class="row" id="etapa_3_prepago_portabilidad_sin_celular2" style="display : none">
		<div class="col s12">
			<p> (Prepago ó Portabilidad) CON CELULAR </p>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_plan_recarga2" id="radio_plan_etapa32" type="radio" />
					<span> Plan de teléfonia </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_plan_recarga2" id="radio_recarga_paquete_etapa32" type="radio" />
					<span> Recarga y/ó paquetes tigo </span>
				</label>
			</p>					
		</div>

		<div class="input-field col s12" style="display : none" id="content_plan_etapa32">
			<select id="plan_etapa32" required>
				<option value="1">Plan A</option>
				<option value="2">PLan B</option>
				<option value="3">Plan C</option>
			</select>
			<label> Planes de teléfonia </label>
		</div>
		<div class="input-field col s12" style="display : none" id="content_recarga_etapa32">
			<div class="row">
				<div class="col s12">
					<select id="recarga_etapa32" required>
						<option value="1">Recarga</option>
						<option value="2">Paquete</option>
					</select>
					<label> Recarga y/ó paquetes tigo </label>
					
				</div>
				<div class="input-field col s12">
					<input id="costo_etapa32" type="number" class="validate" required>
					<label class="active" for="costo_etapa32"> Precio </label>
				</div>
			</div>
		</div>

		<div class="col s12">
			<p>Elegir una opción</p>				
			<p>
				<label>
					<input name="opcion_llevar_agendar2" id="llevarHoy_etapa32" type="radio" />
					<span> Activar </span>
				</label>
			</p>
			<p>
				<label>
					<input name="opcion_llevar_agendar2" id="agendar_etapa32" type="radio" />
					<span> Agendar una cita </span>
				</label>
			</p>					
		</div>

		<div class="col s12" id="fechaagenda_etapa32" style="display : none">
			<div class="row">
				<div class="col s12">
					<input type="text" class="datepicker" id="fecha_etapa32">
					<span> Fecha </span>					
				</div>
				<div class="col s12">
					<input type="text" class="timepicker" id="hora_etapa32">
					<span> Hora </span>
				</div>

			</div>
		</div>

		<div class="col s12 center">
			<button type="button" class="btn btn-primary" onclick="guardar_prepago_portabilidad('{{ csrf_token() }}')">
				Guardar
			</button>			
		</div>
	</div>


</form>

@endsection