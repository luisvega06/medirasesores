@extends('layouts.appadmin')

@section('content')

    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Numero</th>
                <th>Email</th>
                <th>Cargo</th>
                <th>Acción</th>

            </tr>
        </thead>
        
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->phone}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->rol->name}}</td>
                <td>
                   <a href="/admin/users/{{$user->id}}/rol/change">cambiar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    

@endsection