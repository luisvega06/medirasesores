@extends('layouts.appadmin')

@section('content')

<form action="" method="post">
    
    {{ csrf_field() }}

    <input type="text" hidden name="user_id" value="{{$user->id}}">

    <select name="rol_id">
        @foreach($roles as $rol)
            @if($rol->id == $user->rol->id)
                <option value="{{$rol->id}}" selected>{{$rol->name}}</option>
            @else
                <option value="{{$rol->id}}">{{$rol->name}}</option>
            @endif
        @endforeach
    </select>
    
    <button type="submit" class="btn">Cambiar</button>
    
</form>

@endsection