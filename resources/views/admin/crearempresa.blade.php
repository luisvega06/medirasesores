@extends('layouts.appadmin')

@section('content')

	<h3 class="center">Crear una empresa</h3>


	<br>
<br>

{{-- <h1 class="center">hola aca vamos a crear supervisores</h1> --}}

<div class="row container animated lightSpeedIn{{-- bounceInDown --}}">

    <form class="form-horizontal" method="POST" action="/create/empresa">
        {{ csrf_field() }}

        

		<div class="form-group col s6{{ $errors->has('nit') ? ' has-error' : '' }}">
            <label for="nit" class="col-md-4 control-label">Nit</label>

            <div class="col-md-6">
                <input id="nit" type="number" class="form-control" name="nit" value="{{ old('nit') }}" required autofocus>

                @if ($errors->has('nit'))
                <span class="help-block">
                    <strong>{{ $errors->first('nit') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Name</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required >

                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

		<div class="form-group col s6{{ $errors->has('direccion') ? ' has-error' : '' }}">
            <label for="direccion" class="col-md-4 control-label">direccion</label>

            <div class="col-md-6">
                <input id="direccion" type="text" class="form-control" name="direccion" value="{{ old('direccion') }}" required >

                @if ($errors->has('direccion'))
                <span class="help-block">
                    <strong>{{ $errors->first('direccion') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="col-md-4 control-label">Celular</label>

            <div class="col-md-6">
                <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required >

                @if ($errors->has('phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">email</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required >

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>

        
        <div class="form-group col s6{{ $errors->has('sector') ? ' has-error' : '' }}">
            <label for="sector" class="col-md-4 control-label">sector</label>

            <div class="col-md-6">
                <input id="sector" type="text" class="form-control" name="sector" value="{{ old('sector') }}" required >

                @if ($errors->has('sector'))
                <span class="help-block">
                    <strong>{{ $errors->first('sector') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('mod_pago') ? ' has-error' : '' }}">
            <div class="input-field ">
                <select name="modalidad">
                    
                    {{-- @foreach ($datos as $canal ) --}}
                    <option value="Semanal">Semanal</option>
                    <option value="Quincenal">Quincenal</option>
                    <option value="Mensual">Mensual</option>
                   {{--  @endforeach  --}} 
                </select>
                <label>Tiempo de pago</label>
            </div>

        </div>


        <div class="form-group col s6{{ $errors->has('tipo_fondo') ? ' has-error' : '' }}">
            <div class="input-field ">
                <select name="fondo">
                    
                    {{-- @foreach ($datos as $canal ) --}}
                    <option value="Fondo empleado">Fondo empleado</option>
                    <option value="Coperativas">Coperativas</option>
                    <option value="Sistema de libranza">Sistema de libranza</option>
                   {{--  @endforeach  --}} 
                </select>
                <label>Elige tipo de fondo</label>
            </div>

        </div>

        


        <div class="form-group center animated ">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn-floating float btn-primary pulse">
                    <i class="material-icons">send</i>
                </button>
            </div>
        </div>
    </form>
</div>


@endsection