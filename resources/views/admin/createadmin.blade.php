@extends('layouts.appadmin')

@section('content')
<br>
<br>

{{-- <h1 class="center">hola aca vamos a crear supervisores</h1> --}}

<div class="row container">

    <form class="form-horizontal" method="POST" action="/create/supervisor">
        {{ csrf_field() }}

        <div class="form-group col s6{{ $errors->has('canal_id') ? ' has-error' : '' }}">
            <div class="input-field ">
                <select>
                    
                    {{-- @foreach ($datos as $canal ) --}}
                    <option value="1">Dealer</option>
                    <option value="2">Reatil</option>
                    <option value="3">Tienda</option>
                    <option value="4">Home</option>

                   {{--  @endforeach  --}} 
                </select>
                <label>Elige tu canal</label>
            </div>

        </div>



        <div class="form-group col s6{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="col-md-4 control-label">Celular</label>

            <div class="col-md-6">
                <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                @if ($errors->has('phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>
        </div>

        

        <div class="form-group col s6{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Name</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        

        <div class="form-group col s6{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group col s6{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>

        


        <div class="form-group center">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>
    </form>
</div>


@endsection
