@extends('layouts.appadmin')

@section('content')

{{-- @foreach ($usuarios as $usuario )
	<h3 class="center">{{$usuario->name}}</h3>
@endforeach  --}}

@if (sizeof ($empresas) > 0)
	<table class="highlight">
		<thead>
			<tr>
				<th>Nit  			</th>
				<th>Nombre Completo </th>
				<th>Celular 		</th>
				<th>Correo 			</th>
				<th>Dirección 		</th>
				<th>Sector  		</th>
				<th>Fecha creada    </th>				
				<th>Periodo Pago  	</th>
				<th>Tipo Fondo 		</th>	
				<th>Acciones		</th>

			</tr>
		</thead>

		<tbody>
			@foreach($empresas as $empresa)
			<tr>
					<td> {{ $empresa->nit }}         	     </td>
					<td> {{ $empresa->name }}      		     </td>
					<td> {{ $empresa->phone }}               </td>
					<td> {{ $empresa->email }}               </td>					
					<td> {{ $empresa->direccion }}           </td>
					<td> {{ $empresa->sector }}              </td>					
					<td> {{ $empresa->created_at }}          </td>
					<td> {{ $empresa->modalidadpago }}       </td>
					<td> {{ $empresa->tipofondo}} 			 </td>
					<td>
						<a href="" class="btn-floating btn-small waves-effect waves-light red tooltipped" data-position="bottom" data-tooltip="Mostrar ficha">
							<i class="material-icons">visibility</i>
						</a>
						<a class="btn-floating btn-small waves-effect waves-light red tooltipped" data-position="bottom" data-tooltip="Editar">
							<i class="material-icons">create</i>
						</a>

						<a class="btn-floating btn-small waves-effect waves-light red tooltipped" data-position="bottom" data-tooltip="Eliminar">
							<i class="material-icons">delete</i>
						</a>
					</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	<h3 class="center">
		no ahí empresas creadas
	</h3>
@endif


@endsection