@extends('layouts.appadmin')

@section('content')

<h3 class="center">Crear canales</h3>

<form class="form-horizontal" method="POST" action="/create/canal">
        {{ csrf_field() }}

        


	<div class="row container">
		
        <div class="form-group center{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="control-label">Nombre del canal</label>

            <div class="col-md-6">
                <input id="name" type="tel" class="form-control" name="name" value="{{ old('name') }}" required >

                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <br>
        <br>

        <div class="form-group center">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn-floating float btn-primary pulse">
                    <i class="material-icons">send</i>
                </button>
            </div>
        </div>
	</div>

        

        
    </form>


@endsection
