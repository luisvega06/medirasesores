<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referidos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            
            $table->increments('id');
            $table->string('nombre');
            $table->string('cedula');
            $table->string('telefono');
            $table->string('direccion');
            $table->string('correo');
            $table->string('departamento_id');
            $table->string('municipio_id');

            // Creador del referido
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
                        
            // Ofertas: Prepago, PosPago ó portabilidad
            $table->integer('ofertas_id')->unsigned();
            $table->foreign('ofertas_id')->references('id')->on('ofertas');

            $table->Boolean('concelular');

            $table->integer('canal_id')->unsigned();
            $table->foreign('canal_id')->references('id')->on('canal');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('referidos');
    }
}



