<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            
            $table->increments('id');
            $table->string('codigosupervisor')->default(0);
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password');


            $table->integer('rol_id')->unsigned();
            $table->foreign('rol_id')->references('id')->on('rol');

            $table->integer('canal_id')->unsigned();
            $table->foreign('canal_id')->references('id')->on('canal');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
