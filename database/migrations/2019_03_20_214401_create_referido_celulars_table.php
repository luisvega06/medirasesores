<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferidoCelularsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referido_celulars', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            
            $table->increments('id');

            $table->decimal('precio',10,4);

            $table->integer('marcacelulars_id')->unsigned();
            $table->foreign('marcacelulars_id')->references('id')->on('marcacelulars');

            $table->integer('referidos_id')->unsigned();
            $table->foreign('referidos_id')->references('id')->on('referidos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referido_celulars');
    }
}
