<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferidoPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referido_plans', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            
            $table->increments('id');

            $table->decimal('precio',10,4);

            $table->integer('plans_id')->unsigned();
            $table->foreign('plans_id')->references('id')->on('plans');

            $table->integer('referido_celulars_id')->unsigned();
            $table->foreign('referido_celulars_id')->references('id')->on('referido_celulars');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referido_plans');
    }
}
