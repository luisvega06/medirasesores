<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferidoRecargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referido_recargas', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            
            $table->increments('id');

            // Crear tabla con los diferentes tipos de recargas: Saldos, paquetes, promociones y etc.
            $table->integer('referidos_id')->unsigned();
            $table->foreign('referidos_id')->references('id')->on('referidos');

            $table->integer('tiposrecargas_id')->unsigned();
            $table->foreign('tiposrecargas_id')->references('id')->on('tiposrecargas');

            $table->decimal('precio',10,4);

            $table->integer('referido_celulars_id')->unsigned();
            $table->foreign('referido_celulars_id')->references('id')->on('referido_celulars');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referido_recargas');
    }
}
