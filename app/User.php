<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rol_id','canal_id', 'name', 'email', 'password', 'codigosupervisor', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol()
    {
        return $this->hasOne('App\Rol', 'id', 'rol_id');
    }

    public function referidos()
    {
        return $this->hasMany('App\Referido', 'users_id', 'id');
    }
}
