<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referido_recarga extends Model
{
	protected $fillable = [
		'id',
		'referidos_id',
		'tiposrecargas_id',
		'precio',
		'referido_celulars_id',
		
        'created_at',
        'updated_at',
	];
}
