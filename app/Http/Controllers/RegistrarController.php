<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Canal;
use App\User;

class RegistrarController extends Controller
{
  public function index()
  { 
    return view("auth.register", [
      'canales' => Canal::all()
    ]);
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'canal_id'          => 'required|integer|max:10',
          'codigosupervisor'  => 'required|string|max:255',
          'phone'             => 'required|string|max:255',
          'name'              => 'required|string|max:255',
          'email'             => 'required|string|email|max:255|unique:users',
          'password'          => 'required|string|min:6|confirmed',
      ]);
  }


  public function create(Request $request)
  {
    
    $data = $request->input();
    $user = new User($data);
    $user->password = bcrypt($data['password']);
    $user->rol_id = 1;
    $user->save();
    return "Su usuario esta pendite por activacion";
    
    /*
      return User::create([
          'rol_id'            => 1,
          'codigosupervisor'  => $data['codigosupervisor'],
          'canal_id'          => 1,
          'phone'             => $data['phone'],
          'name'              => $data['name'],
          'email'             => $data['email'],
          'password'          => bcrypt($data['password']),
      ]);
      */
  }
}
