<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Rol;

class UsuariosController extends Controller
{
    public function listar()
    {
        $users = User::all();
        return view('admin.users.listar', compact('users'));
    }

    public function changeRolView($id)
    {
        $user = User::find($id);
        $roles = Rol::all();

        if(!empty($user))
        {
            return view('admin.users.changerol', compact('user', 'roles'));
        }
        return redirect('/admin');
    }

    public function changeRol(Request $request)
    {
        $data = $request->input();
        $user = User::find($data['user_id']);
        $user->rol_id = $data['rol_id'];
        $user->save();
        return redirect('/admin/users');
    }
}
