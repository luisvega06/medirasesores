<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    // protected $redirectTo = '/home';
    protected $redirectTo = '/msgnewuser';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'canal_id'          => 'required|integer|max:10',
            'codigosupervisor'  => 'required|string|max:255',
            'phone'             => 'required|string|max:255',
            'name'              => 'required|string|max:255',
            'email'             => 'required|string|email|max:255|unique:users',
            'password'          => 'required|string|min:6|confirmed',
        ]);
    }


    protected function create(array $data)
    {
        return User::create([
            'rol_id'            => 1,
            'codigosupervisor'  => $data['codigosupervisor'],
            'canal_id'          => 1,
            'phone'             => $data['phone'],
            'name'              => $data['name'],
            'email'             => $data['email'],
            'password'          => bcrypt($data['password']),
        ]);
    }
}
