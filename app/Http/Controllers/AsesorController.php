<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\StoreReferidoRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use App\Referido;
use App\Referido_celular;
use App\Referido_plan;
use App\Referido_recarga;
use App\Agendamiento;
use App\Oferta;

use DateTime;
use Illuminate\Support\Facades\Auth;


use DB;
use Hash;
use Response;
use App\User;


class AsesorController extends Controller
{

    public function index()
    {
        
        $referidos_all = DB::table('referidos')
            // ->where('users_id', Auth::User()->$id)
            ->join('towns', 'towns.id', '=', 'referidos.municipio_id')
            ->select('referidos.*', 'towns.name')
            ->orderby('referidos.created_at', 'ASC')->paginate(10);

        $modelsRef =  array();  
        $index = 1;

        foreach ($referidos_all as $referido) {
            $aux = Agendamiento::where('referidos_id', $referido->id)->first();
           

            $referido->idx = $index;
            $object = (object) [
                'referido'  => $referido, 
                'pendiente' => ($aux == null? false : true),
            ];
            array_push($modelsRef, $object);

        }
                    
        // print_r(json_encode($modelsRef));
        // $modelsRef = json_encode($modelsRefInicial);
        // $modelsRef = $modelsRefInicial;
        

        $user = Auth::user();
        
        $datos = $user->referidos()->with('oferta')
        ->selectRaw('ofertas_id, count(*) as total')
        ->groupBy('ofertas_id')
        ->get();

        $ofertas = Oferta::all();


        $dataFinal = [];

        
        foreach($ofertas as $oferta)
        {
            $total = 0;
            foreach($datos as $data)
            {
                if($oferta->id == $data->ofertas_id)
                {
                    $total = $data->total;
                }
            }
            array_push($dataFinal, [
                "oferta" => $oferta,
                "total" => $total
            ]);
        }

        $datos = $dataFinal;
        // return $datos;
        return view('asesor.index', compact('modelsRef','datos'));
    }


    public function create()
    {
        return view('asesor.create');
    }

    public function store(StoreReferidoRequest $request)
    {
    }

    public function pospago_celular(Request $request)
    {
        // return response()->json($request);
        // return "vine al webservice";      
        
        $referido = new Referido();      
        $referido->nombre          = $request->input("nombre");
        $referido->cedula          = $request->input("cedula");
        $referido->telefono        = $request->input("telefono");
        $referido->direccion       = $request->input("direccion");
        $referido->correo          = $request->input("correo");

        $referido->users_id        = Auth::user()->id;

        $referido->departamento_id = 19;
        $referido->municipio_id    = 716;
        $referido->ofertas_id      = $request->input("oferta");
        $referido->concelular      = ($request->input("concelular")? 1 : 0);
        $referido->canal_id        = 1;
        $referido->save();

        
        $referido_celular = new Referido_celular();
        if ($request->input("concelular") == "false") {
            $referido_celular->precio = $request->input("presupuesto_etapa");
        }else{
            $referido_celular->precio = 0;
        }
        if ($request->input("concelular") == "true") {
            echo ("vine aca 112 = ". $request->input("concelular"));
            $referido_celular->marcacelulars_id = $request->input("modelo_etapa");
        }else{
            echo ("vine aca 114");
            $referido_celular->marcacelulars_id = 1;
        }
        $referido_celular->referidos_id     = $referido->id; 

        echo (" || Model Saved= " . $referido_celular );
        $referido_celular->save();

        $referido_plan = new Referido_plan();
        $referido_plan->precio = 1;
        $referido_plan->plans_id = $request->input("plan_etapa");
        $referido_plan->referido_celulars_id = $referido_celular->id;
        $referido_plan->save();

        $llevarHoy_etapa = $request->input("llevarHoy_etapa2");
        if ($llevarHoy_etapa == 'false') {
            $fecha_etapa2       = $request->input("fecha_etapa");
            $hora_etapa2        = $request->input("hora_etapa");
            
            $fecha_model_timestamp = $fecha_etapa2 . " " . $hora_etapa2 . ":00";
            echo ("fecha_model_timestamp: " . $fecha_model_timestamp);
            
            $date = new DateTime($fecha_model_timestamp);
            $timestamp = $date->getTimestamp();
            echo("La returna format: ". $date->getTimestamp());


            $agendamiento = new Agendamiento();
            $agendamiento->fecha = new DateTime($fecha_model_timestamp);
            $agendamiento->referidos_id = $referido->id;
            $agendamiento->save();
            return "successfull";
        }else{
            return "successfull";
        }
    }

    public function prepago_portabilidad_celular(Request $request)
    {
        // return response()->json($request);
        // return "vine al webservice";      
        
        $referido = new Referido();      
        $referido->nombre          = $request->input("nombre");
        $referido->cedula          = $request->input("cedula");
        $referido->telefono        = $request->input("telefono");
        $referido->direccion       = $request->input("direccion");
        $referido->correo          = $request->input("correo");

        $referido->users_id        = Auth::user()->id;

        $referido->departamento_id = 19;
        $referido->municipio_id    = 716;
        $referido->ofertas_id      = $request->input("oferta");
        $referido->concelular      = ($request->input("concelular")? 1 : 0);
        $referido->canal_id        = 1;
        $referido->save();

        
        $referido_celular = new Referido_celular();
        if ($request->input("concelular") == "false") {
            $referido_celular->precio = $request->input("presupuesto_etapa");
        }else{
            $referido_celular->precio = 0;
        }
        if ($request->input("concelular") == "true") {
            echo ("vine aca 112 = ". $request->input("concelular"));
            $referido_celular->marcacelulars_id = $request->input("modelo_etapa");
        }else{
            echo ("vine aca 114");
            $referido_celular->marcacelulars_id = 1;
        }
        $referido_celular->referidos_id     = $referido->id; 

        echo (" || Model Saved= " . $referido_celular );
        $referido_celular->save();

        $conplan = $request->input("conplan");
        if ($conplan == "true") {
            $referido_plan = new Referido_plan();
            $referido_plan->precio = 1;
            $referido_plan->plans_id = $request->input("plan_etapa");
            $referido_plan->referido_celulars_id = $referido_celular->id;
            $referido_plan->save();            
        } else {
            $referido_recarga = new Referido_recarga();
            $referido_recarga->referidos_id         = $referido->id;
            $referido_recarga->tiposrecargas_id     = $request->input("tiporecargapaquete");
            $referido_recarga->precio               = $request->input("precio");
            $referido_recarga->referido_celulars_id = $referido_celular->id;
            $referido_recarga->save();
        }

        $llevarHoy_etapa = $request->input("llevarHoy_etapa");
        if ($llevarHoy_etapa == 'false') {
            $fecha_etapa2       = $request->input("fecha_etapa");
            $hora_etapa2        = $request->input("hora_etapa");
            
            $fecha_model_timestamp = $fecha_etapa2 . " " . $hora_etapa2 . ":00";
            echo ("fecha_model_timestamp: " . $fecha_model_timestamp);
            
            $date = new DateTime($fecha_model_timestamp);
            $timestamp = $date->getTimestamp();
            echo("La returna format: ". $date->getTimestamp());


            $agendamiento = new Agendamiento();
            $agendamiento->fecha = new DateTime($fecha_model_timestamp);
            $agendamiento->referidos_id = $referido->id;
            $agendamiento->save();
            return "successfull";
        }else{
            return "successfull";
        }
    }

    public function show($id)
    {
        $referido = DB::table('referidos')
            ->where('referidos.id', $id)
            ->join('towns', 'towns.id', '=', 'referidos.municipio_id')
            ->join('ofertas', 'ofertas.id', '=', 'referidos.ofertas_id')
            ->select('referidos.*', 'towns.name as name', 'ofertas.name as oferta')
            ->orderby('referidos.created_at', 'ASC')->first();

        $agendamiento = Agendamiento::where('referidos_id', $referido->id)->first();

        $modelref = (object)[
            'referido'  => $referido, 
            'pendiente' => $agendamiento,
        ];
        print_r("<br><br> Data 1= " . json_encode($modelref));

        $referido_celulars = DB::table('referido_celulars')
            ->where('referido_celulars.referidos_id', "=", $id)
            ->join('marcacelulars', 'marcacelulars.id', '=', 'referido_celulars.marcacelulars_id')
            ->select('referido_celulars.*', 'marcacelulars.nombre as name')
            ->first();

        print_r("<br><br> Data 2= " . json_encode($referido_celulars));

        $referido_plans = DB::table('referido_plans')
            ->where('referido_plans.referido_celulars_id', $referido_celulars->id)
            ->join('plans', 'plans.id', '=', 'referido_plans.plans_id')
            ->select('referido_plans.*', 'plans.nombre as name', 'plans.precio as precio')
            ->first();
        print_r("<br><br> Data 3= " . json_encode($referido_plans));

        $referido_recargas = DB::table('referido_recargas')
            ->where('referido_recargas.referido_celulars_id', $referido_celulars->id)
            ->join('tiposrecargas', 'tiposrecargas.id', '=', 'referido_recargas.tiposrecargas_id')
            ->select('referido_recargas.*', 'tiposrecargas.nombre as name')
            ->first();
        print_r("<br><br> Data 4= " . json_encode($referido_recargas));

        return view('asesor.show', compact('modelref', 'referido_celulars', 'referido_plans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

