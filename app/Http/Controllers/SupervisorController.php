<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Canal;
use App\User;
use App\Empresa;
use App\Referido;
use App\Referido_celular;
use App\Referido_plan;
use App\Referido_recarga;
use App\Agendamiento;
use App\Oferta;
use DB;
use Hash;
use Response;
use DateTime;
use Illuminate\Support\Facades\Auth;



class SupervisorController extends Controller
{
	public function vista()
	{
		return view('admin.createadmin');

	}

	public function savecanal(Request $request)
	{
		Canal::create($request->input());
		return "registro exitoso";
	}


	public function save(Request $request)
	{
  	// User::create($request->input());
  	// return "registro exitoso";

		$user = new User();      
		$user->codigosupervisor  	= 0;
		$user->name          		= $request->input("name");  	
		$user->email          		= $request->input("email");
		$user->phone        	    = $request->input("phone");
		$user->password        		= bcrypt($request->input("passwor"));
		$user->rol_id       		= 3;
		$user->canal_id          	= 2;

		$user->save();

		return "registro exitoso";

	}

	public function save_empresa(Request $request)
	{
		$empresa = new Empresa();
		$empresa->nit    		 = $request->input("nit");
		$empresa->name   		 = $request->input("name");
		$empresa->direccion   	 = $request->input("direccion");
		$empresa->phone   		 = $request->input("phone");
		$empresa->email    		 = $request->input("email");
		$empresa->sector    	 = $request->input("sector");
		$empresa->modalidadpago  = $request->input("modalidad");
		$empresa->tipofondo   	 = $request->input("fondo");

		$empresa->save();

		return redirect('/empresas_admin');

	}

	public function all_empresa()
	{
		$empresas = Empresa::all();
		return view("/admin.usuarios", compact('empresas'));
	}

	public function all_referidos()
	{
		$Referido_all = DB::table('referidos')
            // ->where('users_id', Auth::User()->$id)
            ->join('towns', 'towns.id', '=', 'referidos.municipio_id')
            ->select('referidos.*', 'towns.name')
            ->orderby('referidos.created_at', 'ASC')->paginate(10);

        $modelsRef =  array();  
        $index = 1;

        foreach ($Referido_all as $referido) {
            $aux = Agendamiento::where('referidos_id', $referido->id)->first();
           

            $referido->idx = $index;
            $object = (object) [
                'referido'  => $referido, 
                'pendiente' => ($aux == null? false : true),
            ];
            array_push($modelsRef, $object);

        }
        // return var_dump($modelsRef);
        return view('asesor.registro', compact('modelsRef'));

	}



}
