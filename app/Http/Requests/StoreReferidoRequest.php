<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreReferidoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'nombre'              => 'Nombre Completo',
            'cedula'              => 'Cedula',
            'telefono'            => 'Teléfono',
            'direccion'           => 'Dirección',
            'correo'              => 'Correo',
            'oferta'              => 'Oferta',
            'concelular'          => 'Adquirir celular',
        ];
    }

    public function messages()
    {

        return [
            'nombre.required'              => 'El campo Nombre Completo es requerido',
            'cedula.required'              => 'El campo Cedula es requerido',
            'telefono.required'            => 'El campo Teléfono es requerido',
            'direccion.required'           => 'El campo Dirección es requerido',
            'correo.required'              => 'El campo Correo es requerido',
            'oferta.required'              => 'El campo Oferta es requerido',
            'concelular.required'          => 'El campo Adquirir celular es requerido',
        ];
    }

    public function rules()
    {
        return [
            'nombre'     => 'required|string|max:255',
            'cedula'     => 'required|string|unique:referidos',
            'telefono'   => 'required|string|max:20',
            'direccion'  => 'required|string|max:255',
            'correo'     => 'required|string|max:255|unique:referidos',
            'oferta'     => 'required|string|max:255',
            'concelular' => 'required|string|max:255',
        ];

    }
}
