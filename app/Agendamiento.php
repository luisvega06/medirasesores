<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agendamiento extends Model
{


	protected $fillable = [
		'id',
		'fecha',
		'referidos_id',

        'created_at',
        'updated_at',
	];
}

