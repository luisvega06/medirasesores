<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canal extends Model
{
		protected $table = 'canals';
    protected $fillable = [
		'id',
		'name',
        'created_at',
        'updated_at',
	];
}
