<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referido_plan extends Model
{
	protected $fillable = [
		'id',
		'precio',
		'plans_id',
		'referido_celulars_id',
		
        'created_at',
        'updated_at',
	];
}
