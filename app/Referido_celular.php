<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referido_celular extends Model
{
	protected $fillable = [
		'id',
		'precio',
		'marcacelulars_id',
		'referidos_id',
		
        'created_at',
        'updated_at',
	];
}
