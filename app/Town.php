<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
	protected $table = 'towns';

    //Campos que se pueden actualizar
    protected $fillable = [
        'id', 'name',
    ];

    public function state() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\State');
    }
}
