<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	protected $table = 'states';
	
    //Campos que se pueden actualizar
    protected $fillable = [
        'id', 'name',
    ];

    public function users() // Relacion de 1 : N

    {
        return $this->hasMany('Delphos\Town');
    }
}
