<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
    protected $fillable = [
		'id',
		'name',
		'direccion',
		'phone',
		'email',
		'sector',
		'modalidadpago',
		'tipofondo',
        'created_at',
        'updated_at',
	];
}
