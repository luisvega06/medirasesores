<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referido extends Model
{
	protected $fillable = [
		'id',
		'nombre',
		'cedula',
		'telefono',
		'direccion',
		'correo',
		'departamento_id',
		'municipio_id',
		'ofertas_id',
		'ofertas_id',
		'concelular',
		'canal_id',

        'created_at',
        'updated_at',
	];

	public function oferta()
    {
        return $this->hasOne('App\Oferta', 'id', 'ofertas_id');
    }

}
