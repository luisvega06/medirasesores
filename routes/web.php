<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Rutas de Registo y autentificacion por defecto de larevel
 */
//Auth::routes();


Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/msgnewuser', function () {
    return "Gracias por registrarse, debe esperar que se le asigne un rol dentro de la plataforma para poder acceder a ella.";
});

Route::get('/pendiente', function () {
    return "Gracias por registrarse, debe esperar que se le asigne un rol dentro de la plataforma para poder acceder a ella.";
});

Route::resource('/dashboard', 'AsesorController');


// Crear Referido: (Plan PosPago & Con celular)
Route::post('pospago_celular', 'AsesorController@pospago_celular');
Route::post('prepago_portabilidad_celular', 'AsesorController@prepago_portabilidad_celular');

Route::post('create_parte_two', 'AsesorController@create_parte_two');
Route::post('create_parte_three', 'AsesorController@create_parte_three');
Route::get('/create_supervisor','SupervisorController@vista');
Route::post('/create/supervisor','SupervisorController@save');

Route::get('/register', 'RegistrarController@index');
Route::post('/register', 'RegistrarController@create');

Route::get('/login','LoginController@index');
Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout');



Route::get('/crear_canal', function(){
	return view("/admin.crearcanal");
});


Route::post('/create/canal','SupervisorController@savecanal');

Route::get('/crear_empresa', function(){
	return view("/admin.crearempresa");
});

Route::post('/create/empresa','SupervisorController@save_empresa');
Route::get('/empresas_admin','SupervisorController@all_empresa');
Route::get('registros','SupervisorController@all_referidos');



/************************************************************************************
 * Vista de administración
 */

Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return "admin index";
    });

    Route::get('/users', 'Admin\UsuariosController@listar');
    Route::get('/users/{id}/rol/change', 'Admin\UsuariosController@changeRolView');
    Route::post('/users/{id}/rol/change', 'Admin\UsuariosController@changeRol');

});